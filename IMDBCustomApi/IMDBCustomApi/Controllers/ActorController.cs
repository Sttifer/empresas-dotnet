﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMDBCustomApi.Services.Actors;
using IMDBCustomApi.Models.DTOs.Actors;
using Microsoft.AspNetCore.Authorization;

namespace IMDBCustomApi.Controllers
{
    [Route("api/actor")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> GetAll([FromQuery(Name = "page")] int page)
        {
            var actor = await _actorService.GetAll(page);
            return Ok(actor);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ActorDTO>> GetActor(int id)
        {
            var actor = await _actorService.Get(id);

            if (actor == null)
            {
                return NotFound();
            }

            return actor;
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PutActor(int id, ActorDTO actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            var result = await _actorService.Update(id, actor);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<ActorDTO>> PostActor(ActorDTO actor)
        {
            var result = await _actorService.Add(actor);

            return CreatedAtAction("GetActor", new { id = result.Id }, result);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteActor(int id)
        {
            var result = await _actorService.Delete(id);
            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
