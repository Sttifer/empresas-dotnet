﻿using IMDBCustomApi.Models.DTOs;
using IMDBCustomApi.Services.ApplicationUsers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IMDBCustomApi.Controllers
{
    [Authorize(Roles = "user")]
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IApplicationUserService _applicationUserService;

        public UserController(IApplicationUserService applicationUserService)
        {
            _applicationUserService = applicationUserService;
        }

        [HttpPut]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> UpdateApplicationUser(ApplicationUserUpdateRequestDTO applicationUserUpdateDTO)
        {
            var claimNameValue = User.FindFirst(ClaimTypes.Name).Value;
            var result = await _applicationUserService.UpdateApplicationUser(claimNameValue, applicationUserUpdateDTO);

            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpDelete]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> DeleteApplicationUser()
        {
            var claimNameValue = User.FindFirst(ClaimTypes.Name).Value;
            var result = await _applicationUserService.DeleteApplicationUser(claimNameValue);

            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
