﻿using IMDBCustomApi.Models.DTOs;
using IMDBCustomApi.Services.ApplicationUsers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IMDBCustomApi.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/admin")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IApplicationUserService _applicationUserService;

        public AdminController(IApplicationUserService applicationUserService)
        {
            _applicationUserService = applicationUserService;
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateApplicationUser(ApplicationUserUpdateRequestDTO applicationUserUpdateDTO)
        {
            var claimNameValue = User.FindFirst(ClaimTypes.Name).Value;
            var result = await _applicationUserService.UpdateApplicationUser(claimNameValue, applicationUserUpdateDTO);

            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpDelete]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteApplicationUser()
        {
            var claimNameValue = User.FindFirst(ClaimTypes.Name).Value;
            var result = await _applicationUserService.DeleteApplicationUser(claimNameValue);

            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpGet("deleted/users")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetDeletedUsers([FromQuery(Name = "page")] int page)
        {
            var result = await _applicationUserService.GetDeletedApplicationUsers("user", 5, page);

            return Ok(result);
        }
    }
}
