﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IMDBCustomApi.Data;
using IMDBCustomApi.Models;
using IMDBCustomApi.Services.Directors;
using IMDBCustomApi.Specifications;
using IMDBCustomApi.Models.DTOs.Directors;
using Microsoft.AspNetCore.Authorization;

namespace IMDBCustomApi.Controllers
{
    [Route("api/director")]
    [ApiController]
    public class DirectorController : ControllerBase
    {
        private readonly IDirectorService _directorService;

        public DirectorController(IDirectorService directorService)
        {
            _directorService = directorService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<DirectorDTO>>> GetAll([FromQuery(Name = "page")] int page)
        {
            var director = await _directorService.GetAll(page);
            return Ok(director);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<DirectorDTO>> GetDirector(int id)
        {
            var director = await _directorService.Get(id);

            if (director == null)
            {
                return NotFound();
            }

            return director;
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PutDirector(int id, DirectorDTO director)
        {
            if (id != director.Id)
            {
                return BadRequest();
            }

            var result = await _directorService.Update(id, director);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<DirectorDTO>> PostDirector(DirectorDTO director)
        {
            var result = await _directorService.Add(director);

            return CreatedAtAction("GetDirector", new { id = result.Id }, result);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteDirector(int id)
        {
            var result = await _directorService.Delete(id);
            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
