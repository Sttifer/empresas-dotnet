﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMDBCustomApi.Services.Genres;
using IMDBCustomApi.Models.DTOs.Genres;
using Microsoft.AspNetCore.Authorization;

namespace IMDBCustomApi.Controllers
{
    [Route("api/genre")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<GenreDTO>>> GetAll([FromQuery(Name = "page")] int page)
        {
            var genre = await _genreService.GetAll(page);
            return Ok(genre);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<GenreDTO>> GetGenre(int id)
        {
            var genre = await _genreService.Get(id);

            if (genre == null)
            {
                return NotFound();
            }

            return genre;
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PutGenre(int id, GenreDTO genre)
        {
            if (id != genre.Id)
            {
                return BadRequest();
            }

            var result = await _genreService.Update(id, genre);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<GenreDTO>> PostGenre(GenreDTO genre)
        {
            var result = await _genreService.Add(genre);

            return CreatedAtAction("GetGenre", new { id = result.Id }, result);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGenre(int id)
        {
            var result = await _genreService.Delete(id);
            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
