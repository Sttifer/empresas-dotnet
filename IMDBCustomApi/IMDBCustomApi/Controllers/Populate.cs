﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Actors;
using IMDBCustomApi.Models.DTOs.Directors;
using IMDBCustomApi.Models.DTOs.Genres;
using IMDBCustomApi.Models.DTOs.Movies;
using IMDBCustomApi.Services.Actors;
using IMDBCustomApi.Services.Directors;
using IMDBCustomApi.Services.Genres;
using IMDBCustomApi.Services.Movies;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IMDBCustomApi.Controllers
{
    [Route("api/populate")]
    [ApiController]
    public class Populate: ControllerBase
    {
        public IMovieService _movieService;
        public IActorService _actorService;
        public IDirectorService _directorService;
        public IGenreService _genreService;

        public Populate(IMovieService movieService, IActorService actorService, IDirectorService directorService, IGenreService genreService)
        {
            _movieService = movieService;
            _actorService = actorService;
            _directorService = directorService;
            _genreService = genreService;
        }

        [HttpGet("all")]
        public async Task<IActionResult> PopulateAll()
        {
            await PopulateMovie();
            await PopulateActor();
            await PopulateDirector();
            await PopulateGenre();

            return Ok();
        }

        [HttpGet("movie")]
        public async Task<IActionResult> PopulateMovie()
        {
            for (int i = 0; i < 80; i++)
            {
                await _movieService.Add(new MovieAddDTO()
                {
                    Title = "Movie" + i,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vel accumsan mauris. Praesent vitae urna nunc. Nunc condimentum eget mi at accumsan. Aliquam sem purus, semper quis massa tristique, tempus dapibus mauris. Sed ut est nibh. Praesent sollicitudin urna ultrices, viverra nulla non, pellentesque leo. Integer lacinia laoreet ultricies. Fusce vestibulum, quam non placerat eleifend, velit ex iaculis mi, quis varius est lorem ut enim. Sed accumsan augue nec dolor aliquam lobortis. Morbi finibus mi quis placerat efficitur. Maecenas ullamcorper nunc eleifend volutpat dapibus. Vestibulum fermentum libero leo, sed faucibus odio fermentum id. Pellentesque lacinia faucibus est tincidunt blandit. Maecenas in pretium erat. Sed nunc purus, aliquet vitae nibh id, accumsan eleifend nibh."
                });
            }

            return Ok();
        }

        [HttpGet("actor")]
        public async Task<IActionResult> PopulateActor()
        {
            for (int i = 0; i < 80; i++)
            {
                await _actorService.Add(new ActorDTO()
                {
                    Name = "Actor" + i
                });
            }

            return Ok();
        }


        [HttpGet("director")]
        public async Task<IActionResult> PopulateDirector()
        {
            for (int i = 0; i < 80; i++)
            {
                await _directorService.Add(new DirectorDTO()
                {
                    Name = "Director" + i
                });
            }

            return Ok();
        }

        [HttpGet("Genre")]
        public async Task<IActionResult> PopulateGenre()
        {
            for (int i = 0; i < 8; i++)
            {
                await _genreService.Add(new GenreDTO()
                {
                    Title = "Genre" + i
                });
            }

            return Ok();
        }
    }
}
