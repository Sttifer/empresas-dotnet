﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IMDBCustomApi.Models;
using IMDBCustomApi.Services.Movies;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;
using IMDBCustomApi.Specifications;
using IMDBCustomApi.Models.DTOs.Movies;
using IMDBCustomApi.Models.DTOs.MovieRatings;
using Microsoft.AspNetCore.Authorization;

namespace IMDBCustomApi.Controllers
{
    [Route("api/movie")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetAll([FromQuery(Name = "page")] int page, [FromQuery(Name = "director")] string director, [FromQuery(Name = "title")] string title, [FromQuery(Name = "genre")] string genre, [FromQuery(Name = "actors")] string actors)
        {
            var movieDTO = await _movieService.GetAll(page, director, title, genre, actors);

            return Ok(movieDTO);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movieDTO = await _movieService.Get(id);

            if (movieDTO == null)
            {
                return NotFound();
            }

            return movieDTO;
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PutMovie(int id, MovieAddDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            var result = await _movieService.Update(id, movie);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<MovieDTO>> PostMovie(MovieAddDTO movieAddDTO)
        {
            var movieDTO = await _movieService.Add(movieAddDTO);

            return CreatedAtAction("GetMovie", new { id = movieDTO.Id }, movieDTO);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var result = await _movieService.Delete(id);
            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpPost("{id}/rate")]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> RateMovie(MovieRatingDTO dto)
        {
            var result = await _movieService.Rate(dto);
            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
