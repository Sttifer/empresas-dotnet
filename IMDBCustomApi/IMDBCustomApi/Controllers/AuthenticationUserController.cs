﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMDBCustomApi.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using IMDBCustomApi.Services.ApplicationUsers;

namespace IMDBCustomApi.Controllers
{
    [Route("api/authentication/user")]
    [ApiController]
    public class AuthenticationUserController : ControllerBase
    {
        private readonly IApplicationUserService _applicationUserService;

        public AuthenticationUserController(IApplicationUserService applicationUserService)
        {
            _applicationUserService = applicationUserService;
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<ActionResult<ApplicationUserTokenDTO>> RegisterApplicationUser(ApplicationUserRegisterRequestDTO applicationUserRegisterDTO)
        {
            var result = await _applicationUserService.RegisterApplicationUser(applicationUserRegisterDTO, new string[] { "user" });

            if (result.IdentityResult.Succeeded)
            {
                return Ok(result.Token);
            }

            return BadRequest(result.IdentityResult.Errors);
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult<ApplicationUserTokenDTO>> LoginApplicationUser(ApplicationUserLoginRequestDTO applicationUserLoginDTO)
        {
            var result = await _applicationUserService.LoginApplicationUser(applicationUserLoginDTO);

            if (result != null)
            {
                return Ok(result);
            }

            return BadRequest();
        }
    }
}
