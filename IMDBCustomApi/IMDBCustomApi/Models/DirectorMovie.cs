﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models
{
    public class DirectorMovie
    {
        [Key]
        public int Id { get; set; }
        public int DirectorId { get; set; }
        public int MovieId { get; set; }
    }
}
