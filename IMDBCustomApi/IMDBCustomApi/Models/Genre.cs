﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models
{
    public class Genre
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }

        public ICollection<Genre> Genres { get; set; } = new List<Genre>();
    }
}
