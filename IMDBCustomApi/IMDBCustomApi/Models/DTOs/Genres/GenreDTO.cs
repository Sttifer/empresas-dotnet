﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs.Genres
{
    public class GenreDTO
    {
        public GenreDTO() { }

        public GenreDTO(Genre genre)
        {
            Id = genre.Id;
            Title = genre.Title;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }

    }
}
