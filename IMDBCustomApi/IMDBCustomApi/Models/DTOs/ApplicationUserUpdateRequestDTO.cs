﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs
{
    public class ApplicationUserUpdateRequestDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
