﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs.MovieRatings
{
    public class MovieRatingDTO
    {
        public MovieRatingDTO() { }

        public MovieRatingDTO(MovieRating movieRating)
        {
            Id = movieRating.Id;
            UserId = movieRating.UserId;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [Range(0, 4)]
        public int? Value { get; set; }
        [Required]
        public int? UserId { get; set; }
        [Required]
        public int? MovieId { get; set; }
    }
}
