﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs.Directors
{
    public class DirectorDTO
    {
        public DirectorDTO() { }

        public DirectorDTO(Director director)
        {
            Id = director.Id;
            Name = director.Name;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
