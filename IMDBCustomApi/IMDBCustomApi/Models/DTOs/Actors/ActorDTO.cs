﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs.Actors
{
    public class ActorDTO
    {
        public ActorDTO() { }

        public ActorDTO(Actor actor)
        {
            Id = actor.Id;
            Name = actor.Name;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
