﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs
{
    public class ApplicationUserLoginResponseDTO
    {
        public ApplicationUserTokenDTO Token { get; set; }
        public IdentityResult IdentityResult { get; set; }
    }
}
