﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs
{
    public class ApplicationUserDTO
    {
        public ApplicationUserDTO(ApplicationUser user)
        {
            UserName = user.UserName;
            Email = user.Email;
        }

        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
