﻿using IMDBCustomApi.Models.DTOs.Actors;
using IMDBCustomApi.Models.DTOs.Directors;
using IMDBCustomApi.Models.DTOs.Genres;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs.Movies
{
    public class MovieDTO
    {
        public MovieDTO(Movie movie)
        {
            Id = movie.Id;
            Title = movie.Title;
            Description = movie.Description;

            foreach (var item in movie.Actors)
            {
                Actors.Add(new ActorDTO()
                {
                    Id = item.Id,
                    Name = item.Name
                });
            }

            foreach (var item in movie.Directors)
            {
                Directors.Add(new DirectorDTO(item));
            }

            foreach (var item in movie.Genres)
            {
                Genres.Add(new GenreDTO()
                {
                    Id = item.Id,
                    Title = item.Title
                });
            }
        }

        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<DirectorDTO> Directors { get; set; } = new List<DirectorDTO>();
        public ICollection<ActorDTO> Actors { get; set; } = new List<ActorDTO>();
        public ICollection<GenreDTO> Genres { get; set; } = new List<GenreDTO>();

        public int VotesCount { get; set; }
        public int VoteAverage { get; set; }
    }
}
