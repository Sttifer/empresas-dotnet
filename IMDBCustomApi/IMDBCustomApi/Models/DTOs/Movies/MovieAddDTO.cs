﻿using IMDBCustomApi.Models.DTOs.MovieRatings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models.DTOs.Movies
{
    public class MovieAddDTO
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<int> DirectorIds { get; set; } = new List<int>();
        public ICollection<int> ActorIds { get; set; } = new List<int>();
        public ICollection<int> GenreIds { get; set; } = new List<int>();

        public ICollection<int> MovieRatingIds { get; set; }
    }
}
