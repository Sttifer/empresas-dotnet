﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Models
{
    public class ApplicationRole: IdentityRole<int>
    {
    }
}
