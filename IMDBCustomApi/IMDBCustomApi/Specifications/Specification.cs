﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDBCustomApi.Specifications
{
    public class Specification<T> : ISpecification<T>
    {
        public Specification() { }

        public List<Expression<Func<T, bool>>> Filters { get; private set; } = new List<Expression<Func<T, bool>>>();
        public Expression<Func<T, object>> OrderBy { get; private set; }
        public Expression<Func<T, object>> OrderByDescending { get; private set; }
        public List<Expression<Func<T, object>>> Includes { get; private set; } = new List<Expression<Func<T, object>>>();
        public Expression<Func<T, object>> GroupBy { get; private set; }
        public int Take { get; private set; } = 100;
        public int Skip { get; private set; } = 0;

        public Specification<T> AddFilter(Expression<Func<T, bool>> filterExpression)
        {
            Filters.Add(filterExpression);
            return this;
        }

        public Specification<T> AddInclude(Expression<Func<T, object>> includeExpression)
        {
            Includes.Add(includeExpression);
            return this;
        }

        public Specification<T> ApplyOrderBy(Expression<Func<T, object>> orderByExpression)
        {
            OrderBy = orderByExpression;
            return this;
        }

        public Specification<T> ApplyOrderByDescending(Expression<Func<T, object>> orderByDescendingExpression)
        {
            OrderByDescending = orderByDescendingExpression;
            return this;
        }

        public Specification<T> ApplyGroupBy(Expression<Func<T, object>> groupByExpression)
        {
            GroupBy = groupByExpression;
            return this;
        }

        public Specification<T> ApplyPagination(int pageSize, int page)
        {
            Take = pageSize > 0 ? pageSize : Take;
            Skip = Math.Max(Skip, (page - 1) * pageSize);
            return this;
        }
    }
}
