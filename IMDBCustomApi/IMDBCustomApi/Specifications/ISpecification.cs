﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDBCustomApi.Specifications
{
    public interface ISpecification<T>
    {
        List<Expression<Func<T, bool>>> Filters { get; }
        Expression<Func<T, object>> OrderBy { get; }
        Expression<Func<T, object>> OrderByDescending { get; }
        List<Expression<Func<T, object>>> Includes { get; }
        Expression<Func<T, object>> GroupBy { get; }
        int Skip { get; }
        int Take { get; }
    }
}
