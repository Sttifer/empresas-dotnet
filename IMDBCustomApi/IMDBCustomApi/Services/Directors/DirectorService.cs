﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Directors;
using IMDBCustomApi.Repositories;
using IMDBCustomApi.Repositories.Directors;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Directors
{
    public class DirectorService: IDirectorService
    {
        private IDirectorRepository _repository;

        public DirectorService(IDirectorRepository repository)
        {
            _repository = repository;
        }

        public async Task<DirectorDTO> Add(DirectorDTO dto)
        {
            var director = new Director()
            {
                Name = dto.Name
            };

            var directorResult = await _repository.Add(director);

            if (directorResult == null)
            {
                return null;
            }

            var directorDTO = new DirectorDTO(directorResult);

            return directorDTO;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<DirectorDTO> Get(int id)
        {
            var directorResult = await _repository.Get(id);

            if (directorResult == null)
            {
                return null;
            }

            var directorDTO = new DirectorDTO(directorResult);

            return directorDTO;
        }

        public async Task<IEnumerable<DirectorDTO>> GetAll(int page)
        {
            var specification = new Specification<Director>().ApplyPagination(8, page);

            var directorResult = await _repository.GetAll(specification);

            var directorDTOs = new List<DirectorDTO>();
            foreach (var item in directorResult)
            {
                directorDTOs.Add(new DirectorDTO(item));
            }

            return directorDTOs;
        }

        public async Task<bool> Update(int id, DirectorDTO dto)
        {
            var director = new Director()
            {
                Id = dto.Id,
                Name = dto.Name
            };

            return await _repository.Update(id, director);
        }
    }
}
