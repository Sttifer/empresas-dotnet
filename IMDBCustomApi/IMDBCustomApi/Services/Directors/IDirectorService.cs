﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Directors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Directors
{
    public interface IDirectorService
    {
        Task<bool> Delete(int id);
        Task<DirectorDTO> Get(int id);
        Task<IEnumerable<DirectorDTO>> GetAll(int page);
        Task<DirectorDTO> Add(DirectorDTO entity);
        Task<bool> Update(int id, DirectorDTO entity);
    }
}
