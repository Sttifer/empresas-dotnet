﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.MovieRatings;
using IMDBCustomApi.Models.DTOs.Movies;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Movies
{
    public interface IMovieService
    {
        Task<bool> Delete(int id);
        Task<MovieDTO> Get(int id);
        Task<IEnumerable<MovieDTO>> GetAll(int page, string director, string title, string genre, string actor);
        Task<MovieDTO> Add(MovieAddDTO entity);
        Task<bool> Update(int id, MovieAddDTO entity);
        Task<bool> Rate(MovieRatingDTO dto);
    }
}
