﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.MovieRatings;
using IMDBCustomApi.Models.DTOs.Movies;
using IMDBCustomApi.Repositories.Movies;
using IMDBCustomApi.Services.MovieRatings;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Movies
{
    public class MovieService : IMovieService
    {
        private IMovieRepository _repository;
        private IMovieRatingService _movieRatingService;

        public MovieService(IMovieRepository repository, IMovieRatingService movieRatingService)
        {
            _repository = repository;
            _movieRatingService = movieRatingService;
        }

        public async Task<MovieDTO> Add(MovieAddDTO dto)
        {
            var movie = new Movie()
            {
                Title = dto.Title,
                Description = dto.Description
            };

            var movieResult = await _repository.Add(movie, dto.ActorIds, dto.DirectorIds, dto.GenreIds);

            if (movieResult == null)
            {
                return null;
            }

            var movieDTO = new MovieDTO(movieResult);

            return movieDTO;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<MovieDTO> Get(int id)
        {
            var movieResult = await _repository.Get(id);

            if (movieResult == null)
            {
                return null;
            }

            var movieDTO = new MovieDTO(movieResult);
            movieDTO.VoteAverage = GetVoteAverage(movieResult.MovieRatings);
            movieDTO.VotesCount = movieResult.MovieRatings.Count();

            return movieDTO;
        }

        public async Task<IEnumerable<MovieDTO>> GetAll(int page, string director, string title, string genre, string actor)
        {
            var specification = new Specification<Movie>().ApplyPagination(8, page);


            if (director != null)
            {
                specification.AddFilter(e => e.Directors.Any(d => d.Name == director));
            }

            if (title != null)
            {
                specification.AddFilter(e => e.Title == title);
            }

            if (genre != null)
            {
                specification.AddFilter(e => e.Genres.Any(a => a.Title == genre));
            }

            if (actor != null)
            {
                specification.AddFilter(e => e.Actors.Any(a => a.Name == actor));
            }
            var movieResult = await _repository.GetAll(specification);

            var movieDTOs = new List<MovieDTO>();
            foreach (var item in movieResult)
            {
                var movieDTO = new MovieDTO(item);
                movieDTO.VoteAverage = GetVoteAverage(item.MovieRatings);
                movieDTO.VotesCount = item.MovieRatings.Count();
                movieDTOs.Add(movieDTO);
            }

            return movieDTOs;
        }

        public async Task<bool> Rate(MovieRatingDTO dto)
        {
            var result = await _movieRatingService.Add(dto);

            if (result == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ChangeRate(MovieRatingDTO dto)
        {
            var result = await _movieRatingService.Update(dto.MovieId.Value, dto);

            return result;
        }

        public async Task<bool> Update(int id, MovieAddDTO dto)
        {
            var movie = new Movie()
            {
                Id = dto.Id,
                Title = dto.Title,
                Description = dto.Description
            };

            return await _repository.Update(id, movie);
        }

        private int GetVoteAverage(IEnumerable<MovieRating> movieRatings)
        {
            var result = 0;
            foreach (var item in movieRatings)
            {
                result += item.Value;
            }

            result = result == 0 ? 0 : result / movieRatings.Count();

            return result;
        }
    }
}
