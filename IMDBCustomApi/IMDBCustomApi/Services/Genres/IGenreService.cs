﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Genres;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Genres
{
    public interface IGenreService
    {
        Task<bool> Delete(int id);
        Task<GenreDTO> Get(int id);
        Task<IEnumerable<GenreDTO>> GetAll(int page);
        Task<GenreDTO> Add(GenreDTO entity);
        Task<bool> Update(int id, GenreDTO entity);
    }
}
