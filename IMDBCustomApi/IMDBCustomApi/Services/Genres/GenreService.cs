﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Genres;
using IMDBCustomApi.Repositories.Genres;
using IMDBCustomApi.Specifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Genres
{
    public class GenreService : IGenreService
    {
        private IGenreRepository _repository;

        public GenreService(IGenreRepository repository)
        {
            _repository = repository;
        }

        public async Task<GenreDTO> Add(GenreDTO dto)
        {
            var genre = new Genre()
            {
                Title = dto.Title
            };

            var genreResult = await _repository.Add(genre);

            if (genreResult == null)
            {
                return null;
            }

            var genreDTO = new GenreDTO(genreResult);

            return genreDTO;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<GenreDTO> Get(int id)
        {
            var genreResult = await _repository.Get(id);
            
            if (genreResult == null)
            {
                return null;
            }

            var genreDTO = new GenreDTO(genreResult);

            return genreDTO;
        }

        public async Task<IEnumerable<GenreDTO>> GetAll(int page)
        {
            var specification = new Specification<Genre>().ApplyPagination(8, page);

            var genreResult = await _repository.GetAll(specification);

            var genreDTOs = new List<GenreDTO>();
            foreach (var item in genreResult)
            {
                genreDTOs.Add(new GenreDTO(item));
            }

            return genreDTOs;
        }

        public async Task<bool> Update(int id, GenreDTO dto)
        {
            var genre = new Genre()
            {
                Id = dto.Id,
                Title = dto.Title
            };

            return await _repository.Update(id, genre);
        }
    }
}
