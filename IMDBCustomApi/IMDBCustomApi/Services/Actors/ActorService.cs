﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Actors;
using IMDBCustomApi.Repositories.Actors;
using IMDBCustomApi.Specifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Actors
{
    public class ActorService : IActorService
    {
        private IActorRepository _repository;

        public ActorService(IActorRepository repository)
        {
            _repository = repository;
        }

        public async Task<ActorDTO> Add(ActorDTO dto)
        {
            var actor = new Actor()
            {
                Name = dto.Name
            };

            var actorResult = await _repository.Add(actor);

            if (actorResult == null)
            {
                return null;
            }

            var actorDTO = new ActorDTO(actorResult);

            return actorDTO;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<ActorDTO> Get(int id)
        {
            var actorResult = await _repository.Get(id);

            if (actorResult == null)
            {
                return null;
            }

            var actorDTO = new ActorDTO(actorResult);

            return actorDTO;
        }

        public async Task<IEnumerable<ActorDTO>> GetAll(int page)
        {
            var specification = new Specification<Actor>().ApplyPagination(8, page);

            var actorResult = await _repository.GetAll(specification);

            var actorDTOs = new List<ActorDTO>();
            foreach (var item in actorResult)
            {
                actorDTOs.Add(new ActorDTO(item));
            }

            return actorDTOs;
        }

        public async Task<bool> Update(int id, ActorDTO dto)
        {
            var actor = new Actor()
            {
                Id = dto.Id,
                Name = dto.Name
            };

            return await _repository.Update(id, actor);
        }
    }
}
