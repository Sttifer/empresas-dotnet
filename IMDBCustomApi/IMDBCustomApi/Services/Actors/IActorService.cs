﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.Actors;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Actors
{
    public interface IActorService
    {
        Task<bool> Delete(int id);
        Task<ActorDTO> Get(int id);
        Task<IEnumerable<ActorDTO>> GetAll(int page);
        Task<ActorDTO> Add(ActorDTO entity);
        Task<bool> Update(int id, ActorDTO entity);
    }
}
