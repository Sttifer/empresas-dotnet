﻿using IMDBCustomApi.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.ApplicationUsers
{
    public interface IApplicationUserService
    {
        public Task<ApplicationUserLoginResponseDTO> RegisterApplicationUser(ApplicationUserRegisterRequestDTO applicationUserRegisterDTO, IEnumerable<string> roles);
        public Task<ApplicationUserTokenDTO> LoginApplicationUser(ApplicationUserLoginRequestDTO applicationUserLoginDTO);
        public Task<bool> UpdateApplicationUser(string username, ApplicationUserUpdateRequestDTO applicationUserUpdateDTO);
        public Task<bool> DeleteApplicationUser(string username);
        public Task<IEnumerable<ApplicationUserDTO>> GetDeletedApplicationUsers(string role, int pageSize, int page);
    }
}
