﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs;
using IMDBCustomApi.Services.Tokens;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.ApplicationUsers
{
    public class ApplicationUserService : IApplicationUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ITokenService _tokenService;

        public ApplicationUserService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ITokenService tokenService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
        }

        public async Task<ApplicationUserLoginResponseDTO> RegisterApplicationUser(ApplicationUserRegisterRequestDTO applicationUserRegisterDTO, IEnumerable<string> roles)
        {
            var user = new ApplicationUser()
            {
                UserName = applicationUserRegisterDTO.UserName
            };

            var applicationUserResponseDTO = new ApplicationUserLoginResponseDTO();

            var userResult = await _userManager.CreateAsync(user, applicationUserRegisterDTO.Password);
            applicationUserResponseDTO.IdentityResult = userResult;

            if (!userResult.Succeeded)
            {
                return applicationUserResponseDTO;
            }

            var rolesResult = await _userManager.AddToRolesAsync(user, roles);
            if (!rolesResult.Succeeded)
            {
                return applicationUserResponseDTO;
            }

            applicationUserResponseDTO.IdentityResult = rolesResult;

            applicationUserResponseDTO.Token = _tokenService.GenerateToken(user, roles);
            return applicationUserResponseDTO;
        }

        public async Task<bool> UpdateApplicationUser(string username, ApplicationUserUpdateRequestDTO applicationUserUpdateDTO)
        {
            var user = await _userManager.Users.Where(ctx => ctx.UserName == username && ctx.IsDeleted == false).FirstOrDefaultAsync();
            if (user == null)
            {
                return false;
            }
            
            user.Email = applicationUserUpdateDTO.Email;

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteApplicationUser(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return false;
            }

            user.IsDeleted = true;

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return true;
            }

            return false;
        }

        public async Task<ApplicationUserTokenDTO> LoginApplicationUser(ApplicationUserLoginRequestDTO applicationUserLoginDTO)
        {
            var user = await _userManager.FindByNameAsync(applicationUserLoginDTO.UserName);
            if (user == null)
            {
                return null;
            }

            var userResult = await _signInManager.CheckPasswordSignInAsync(user, applicationUserLoginDTO.Password, false);
            if (!userResult.Succeeded)
            {
                return null;
            }

            var rolesResult = await _userManager.GetRolesAsync(user);
            
            return _tokenService.GenerateToken(user, rolesResult);
        }

        public async Task<IEnumerable<ApplicationUserDTO>> GetDeletedApplicationUsers(string role, int pageSize, int page)
        {
            var usersInRole = await _userManager.GetUsersInRoleAsync(role);
            var users = usersInRole.Where(ctx => ctx.IsDeleted == true).OrderBy(ctx => ctx.UserName).Skip((page - 1) * pageSize).Take(pageSize).Select(ctx => new ApplicationUserDTO(ctx));
            return users;
        }
    }
}
