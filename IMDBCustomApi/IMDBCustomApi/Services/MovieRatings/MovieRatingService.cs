﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.MovieRatings;
using IMDBCustomApi.Repositories.MovieRatings;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.MovieRatings
{
    public class MovieRatingService: IMovieRatingService
    {
        private IMovieRatingRepository _repository;

        public MovieRatingService(IMovieRatingRepository repository)
        {
            _repository = repository;
        }

        public async Task<MovieRatingDTO> Add(MovieRatingDTO dto)
        {
            var movieRating = new MovieRating()
            {
                Value = dto.Value.Value,
                UserId = dto.UserId.Value,
                MovieId = dto.MovieId.Value,
            };

            var movieRatingResult = await _repository.Add(movieRating);

            if (movieRatingResult == null)
            {
                return null;
            }

            var movieRatingDTO = new MovieRatingDTO(movieRatingResult);

            return movieRatingDTO;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<MovieRatingDTO> Get(int id)
        {
            var movieRatingResult = await _repository.Get(id);

            if (movieRatingResult == null)
            {
                return null;
            }

            var movieRatingDTO = new MovieRatingDTO(movieRatingResult);

            return movieRatingDTO;
        }

        public async Task<IEnumerable<MovieRatingDTO>> GetAll(int page)
        {
            var specification = new Specification<MovieRating>().ApplyPagination(8, page);

            var movieRatingResult = await _repository.GetAll(specification);

            var movieRatingDTOs = new List<MovieRatingDTO>();
            foreach (var item in movieRatingResult)
            {
                movieRatingDTOs.Add(new MovieRatingDTO(item));
            }

            return movieRatingDTOs;
        }

        public async Task<bool> Update(int id, MovieRatingDTO dto)
        {
            var movieRating = new MovieRating()
            {
                Id = dto.Id,
                Value = dto.Value.Value,
                UserId = dto.UserId.Value,
                MovieId = dto.MovieId.Value,
            };

            return await _repository.Update(id, movieRating);
        }
    }
}
