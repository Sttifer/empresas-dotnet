﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Models.DTOs.MovieRatings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.MovieRatings
{
    public interface IMovieRatingService
    {
        Task<bool> Delete(int id);
        Task<MovieRatingDTO> Get(int id);
        Task<IEnumerable<MovieRatingDTO>> GetAll(int page);
        Task<MovieRatingDTO> Add(MovieRatingDTO entity);
        Task<bool> Update(int id, MovieRatingDTO entity);
    }
}
