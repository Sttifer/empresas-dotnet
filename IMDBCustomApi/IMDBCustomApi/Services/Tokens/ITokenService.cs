﻿using IMDBCustomApi.Models.DTOs;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IMDBCustomApi.Services.Tokens
{
    public interface ITokenService
    {
        ApplicationUserTokenDTO GenerateToken(IdentityUser<int> user, IEnumerable<String> roles);
    }
}
