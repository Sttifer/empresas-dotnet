﻿using IMDBCustomApi.Data;
using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Actors
{
    public class ActorRepository : IActorRepository
    {
        private ApplicationDbContext _context;

        public ActorRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Actor> Add(Actor entity)
        {
            await _context.Actors.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.Actors.FindAsync(id);

            if (entity == null)
            {
                return false;
            }

            _context.Actors.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<Actor> Get(int id)
        {
            return await _context.Actors.FindAsync(id);
        }

        public async Task<IEnumerable<Actor>> GetAll(ISpecification<Actor> specification)
        {
            var ctx = _context.Actors.AsQueryable();
            return await SpecificationEvaluator<Actor>.GetQuery(ctx, specification).ToListAsync();
        }

        public async Task<bool> Update(int id, Actor entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool Exists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
