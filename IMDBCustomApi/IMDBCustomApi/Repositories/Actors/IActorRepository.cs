﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Actors
{
    public interface IActorRepository
    {
        Task<bool> Delete(int id);
        Task<Actor> Get(int id);
        Task<IEnumerable<Actor>> GetAll(ISpecification<Actor> specification);
        Task<Actor> Add(Actor entity);
        Task<bool> Update(int id, Actor entity);
    }
}
