﻿using IMDBCustomApi.Data;
using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Movies
{
    public class MovieRepository : IMovieRepository
    {
        private ApplicationDbContext _context;

        public MovieRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> Add(Movie entity, IEnumerable<int> actorsId, IEnumerable<int> directorsId, IEnumerable<int> genresId)
        {
            entity.Actors = new List<Actor>(await GetReferences<Actor>(actorsId));
            entity.Directors = new List<Director>(await GetReferences<Director>(directorsId));
            entity.Genres = new List<Genre>(await GetReferences<Genre>(genresId));

            await _context.Movies.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        private async Task<IEnumerable<T>> GetReferences<T>(IEnumerable<int> ids) where T : class
        {
            var list = new List<T>();
            foreach (var id in ids)
            {
                var itemRef = await _context.Set<T>().FindAsync(id);
                list.Add(itemRef);
            }

            return list;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.Movies.FindAsync(id);

            if (entity == null)
            {
                return false;
            }

            _context.Movies.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<Movie> Get(int id)
        {
            return await _context.Movies
                .Include(ctx => ctx.Actors)
                .Include(ctx => ctx.Directors)
                .Include(ctx => ctx.Genres)
                .Include(ctx => ctx.MovieRatings)
                .FirstOrDefaultAsync(ctx => ctx.Id == id);
        }

        public async Task<IEnumerable<Movie>> GetAll(ISpecification<Movie> specification)
        {
            var ctx = _context.Movies.AsQueryable()
                .Include(ctx => ctx.Actors)
                .Include(ctx => ctx.Directors)
                .Include(ctx => ctx.Genres)
                .Include(ctx => ctx.MovieRatings)
                .OrderByDescending(ctx => ctx.MovieRatings
                    .Where(rateCtx => rateCtx.MovieId == ctx.Id)
                    .Select(rateCtx => rateCtx)
                    .Count())
                .ThenBy(ctx => ctx.Title);
            return await SpecificationEvaluator<Movie>.GetQuery(ctx, specification).ToListAsync();
        }

        public async Task<bool> Update(int id, Movie entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool Exists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
