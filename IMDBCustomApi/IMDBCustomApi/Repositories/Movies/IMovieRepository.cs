﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Movies
{
    public interface IMovieRepository
    {
        Task<bool> Delete(int id);
        Task<Movie> Get(int id);
        Task<IEnumerable<Movie>> GetAll(ISpecification<Movie> specification);
        Task<Movie> Add(Movie entity, IEnumerable<int> actorsId, IEnumerable<int> directorsId, IEnumerable<int> genresId);
        Task<bool> Update(int id, Movie entity);
    }
}
