﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Genres
{
    public interface IGenreRepository
    {
        Task<bool> Delete(int id);
        Task<Genre> Get(int id);
        Task<IEnumerable<Genre>> GetAll(ISpecification<Genre> specification);
        Task<Genre> Add(Genre entity);
        Task<bool> Update(int id, Genre entity);
    }
}
