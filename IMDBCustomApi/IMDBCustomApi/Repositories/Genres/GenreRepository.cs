﻿using IMDBCustomApi.Data;
using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Genres

{
    public class GenreRepository : IGenreRepository
    {
        private ApplicationDbContext _context;

        public GenreRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Genre> Add(Genre entity)
        {
            await _context.Genres.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.Genres.FindAsync(id);

            if (entity == null)
            {
                return false;
            }

            _context.Genres.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<Genre> Get(int id)
        {
            return await _context.Genres.FindAsync(id);
        }

        public async Task<IEnumerable<Genre>> GetAll(ISpecification<Genre> specification)
        {
            var ctx = _context.Genres.AsQueryable();
            return await SpecificationEvaluator<Genre>.GetQuery(ctx, specification).ToListAsync();
        }

        public async Task<bool> Update(int id, Genre entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool Exists(int id)
        {
            return _context.Genres.Any(e => e.Id == id);
        }
    }
}
