﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Directors
{
    public interface IDirectorRepository
    {
        Task<bool> Delete(int id);
        Task<Director> Get(int id);
        Task<IEnumerable<Director>> GetAll(ISpecification<Director> specification);
        Task<Director> Add(Director entity);
        Task<bool> Update(int id, Director entity);
    }
}
