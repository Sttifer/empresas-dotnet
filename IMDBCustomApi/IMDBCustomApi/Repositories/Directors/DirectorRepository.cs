﻿using IMDBCustomApi.Data;
using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.Directors
{
    public class DirectorRepository : IDirectorRepository
    {
        private ApplicationDbContext _context;

        public DirectorRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Director> Add(Director entity)
        {
            await _context.Directors.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.Directors.FindAsync(id);

            if (entity == null)
            {
                return false;
            }

            _context.Directors.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<Director> Get(int id)
        {
            return await _context.Directors.FindAsync(id);
        }

        public async Task<IEnumerable<Director>> GetAll(ISpecification<Director> specification)
        {
            var ctx = _context.Directors.AsQueryable();
            return await SpecificationEvaluator<Director>.GetQuery(ctx, specification).ToListAsync();
        }

        public async Task<bool> Update(int id, Director entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool Exists(int id)
        {
            return _context.Directors.Any(e => e.Id == id);
        }
    }
}
