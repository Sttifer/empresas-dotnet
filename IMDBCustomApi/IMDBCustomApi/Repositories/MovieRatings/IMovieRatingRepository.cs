﻿using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.MovieRatings
{
    public interface IMovieRatingRepository
    {
        Task<MovieRating> Add(MovieRating entity);
        Task<bool> Delete(int id);
        Task<MovieRating> Get(int id);
        Task<IEnumerable<MovieRating>> GetAll(ISpecification<MovieRating> specification);
        Task<bool> Update(int id, MovieRating entity);
    }
}
