﻿using IMDBCustomApi.Data;
using IMDBCustomApi.Models;
using IMDBCustomApi.Specifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IMDBCustomApi.Repositories.MovieRatings
{
    public class MovieRatingRepository: IMovieRatingRepository
    {
        private ApplicationDbContext _context;

        public MovieRatingRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<MovieRating> Add(MovieRating entity)
        {
            await _context.MovieRatings.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _context.MovieRatings.FindAsync(id);

            if (entity == null)
            {
                return false;
            }

            _context.MovieRatings.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<MovieRating> Get(int id)
        {
            return await _context.MovieRatings.FindAsync(id);
        }

        public async Task<IEnumerable<MovieRating>> GetAll(ISpecification<MovieRating> specification)
        {
            var ctx = _context.MovieRatings.AsQueryable();
            return await SpecificationEvaluator<MovieRating>.GetQuery(ctx, specification).ToListAsync();
        }

        public async Task<bool> Update(int id, MovieRating entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool Exists(int id)
        {
            return _context.MovieRatings.Any(e => e.Id == id);
        }
    }
}
